ESP32 GRBL CNC controller
=========================

This is a CNC controller board that is based on [GRBL-ESP32](https://github.com/bdring/Grbl_Esp32/).
It has some advantages:

* Includes a MicroSD card.
* All pins that can be broken out are.
* Some pins that can't be broken out are too, with appropriate pulldowns.
* Allows for a 3.3V LDO for the LOLIN ESP32 Lite.
* Allows for a 3A 5V step-down converter for powering servos.
* Other stuff I forget now.


## Building

To build the project, just install `kibot` and run `kibot`. That will
create the Gerber files in the `gerbers` directory.


## Pinouts

The pin mapping is as follows:

| ESP32 Pin | Function |
| --: | --- |
|   2 | ENABLE steppers when LOW. |
|  22 | X stepper driver `step` pin. |
|  25 | X stepper driver `dir` pin. |
|  26 | Y stepper driver `step` pin. |
|  27 | Y stepper driver `dir` pin. |
|  22 | Z stepper driver `step` pin. |
|  25 | Z stepper driver `dir` pin. |
|   4 | Broken out. |
|  13 | Broken out. |
|  14 | Broken out. |
|  16 | Broken out. |
|  17 | Broken out. |
|  32 | Broken out. |
|  33 | Broken out. |



## Images

Here are some images of the PCB. The SVGs are the most up to date:


![](misc/ESP32_CNC.png)
![](misc/ESP32_CNC-top.svg)
![](misc/ESP32_CNC-bottom.svg)
